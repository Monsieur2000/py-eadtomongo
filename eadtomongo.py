#!./.venv/bin/python
# -*- coding: utf-8 -*-

import lxml.etree as ET
import xmltodict, json
from pymongo import MongoClient
import datetime
import argparse
import getpass
import os
from dotenv import load_dotenv
import re
import logging

class Compteur():
    def __init__(self) -> None:
        self.ud_modif = 0
        self.ud_created = 0
        self.begin_time = datetime.datetime.now()

def main(args):

    # initialisation du compteur
    global compteur
    compteur = Compteur()

    # Config du loggeur
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s %(levelname)s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        handlers=[
            logging.FileHandler("eadtomongo.log"),
            logging.StreamHandler()
        ]
    )

    # utilisation d'un fichier .env pour le pwd et l'user
    load_dotenv()
    DB_USER = os.getenv('DB_USER')
    DB_PWD = os.getenv('DB_PWD')


    # erreur si -e est mis sans -c
    if args.e and not args.c:
        exit("Une cote doit être spécifiée si -e est mentionné")

    # création du dictionnaire avec les uds
    o = create_dict(args.f)

    # coonexion à la base de données et récupération du client Mango
    db = connect_db(DB_USER, DB_PWD)

    # Evaluation et traitement des uds en fonction des options
    eval_ud(o, db, args.c, args.e)

    # Affichage des résultats
    resultats(args.c)


def connect_db(user=None,pwd=None):

    # Récupération du login de connection à la base de donnée
    if not user:
        user = input("MongoDB user : ")

    if not pwd:
        pwd = getpass.getpass(prompt="MongoDB password : ")

    # Création du client de la base Mongo
    mClient = MongoClient(
        host="130.223.28.115",
        port=27017,
        username=user,
        password=pwd,
        authSource="admin"
    )

    db = mClient['fjme-db']

    return db

def create_dict(xml_file):
    # Transformation de l'EAD en un fichier XML simplifié
    dom = ET.parse(xml_file)
    xslt = ET.parse('ead_vers_json.xsl')
    transform = ET.XSLT(xslt)
    newdom = transform(dom)

    # On transforme le XML en un dictionnaire python
    return xmltodict.parse(newdom)

def corrSpaces(text):
    text = re.sub(" ,", ",", text)
    text = re.sub("' ", "'", text)
    text = re.sub(r" \.",".", text)

    text = re.sub(r"\( ", "(", text)
    text = re.sub(r" \)", ")", text)

    return text

def convert_ud(ud, db,ud_id):

    # création des deux collection pour le client Mongo (db)

    collec = db.uds
    docs_num = db.objets

    # insertion de la date de modification/création
    ud['lastModif'] = datetime.datetime.now(datetime.UTC)
    # Ajout du niveau d'accès au documents numériques
    # FIXME: ça l'ajoute à chaque UD même celles qui ne contiennent pas de documents numériques
    #ud['docs_num_acces'] = 300
    if "docs_num_acces" in ud:
        ud['docs_num_acces'] = int(ud['docs_num_acces'])

    # On ajoute les infos sur les documents numériques aux UD qui en ont
    if not 'enfants' in ud: # on considère que les ud de bas niveau (qui ont pas d'enfants)
        query = {"cote" : ud_id, "objecttype" : "digital", "localisation" : {"$regex" : "diffusion"}} # on ne récupère que les infos des documents au format diffusion
        projection = {"_id" : 0, "lastModif" : 0} # on omet les champs _id et lastModif
        # nombre de résultats de la requête
        nb_docs = docs_num.count_documents(query)
        # si la requête donne une réponse, on ajout les infos à l'UD
        if nb_docs > 0:
            docs = docs_num.find(query, projection=projection) # on fait la requête
            ud['docs_num'] = []
            for d in docs:
                ud['docs_num'].append(d)

    # XSLT et la gestion des espaces et tabul est problématique quand on veut faire du Markdown avec.
    # cette partie sert à supprimer les espces en trop devant les points, les virgules et les apostrophes
    list_champs = ("bioghist", "processinfo", "relatedmaterial", "scopecontent", "custodhist")
    for c in list_champs:
        if c in ud:
            try:
                ud[c] = corrSpaces(ud[c])
            except:
                print("Erreur :")
                print(ud['_id'])
                print(ud[c])

    x = collec.replace_one({"_id" : ud_id}, ud, True) # On entre l'UD dans la base de données

    # On print si l'UD a été crée ou modifiée et on incrémente les compteurs
    if x.modified_count:
        logging.info(f"{ud_id} modifiée")
        compteur.ud_modif += 1
    else:
        logging.info(f"{x.upserted_id} ajoutée")
        compteur.ud_created += 1

def eval_ud(o,db,cote,expansion):
    # correction id si on demande "-c fiche"
    if cote == 'fiche':
        cote = o['root']['c'][0]['_id']

    # On boucle sur chaque UD du dictionaire python
    for ud in o['root']['c']:
        # FIXME ça donne une erreur avec la Dummy UD s'il y a seulement une fiche fonds. Mais bon le script continue quand même
        # récupération de l'id de l'UD
        try:
            ud_id = ud['_id']
        except:
            logging.error(f"Erreur avec : {ud}")
            continue
        # si pas de cote spécifiée ou si une seule cote sans expansion
        if cote is None or (cote == ud['_id'] and expansion is False):
            convert_ud(ud, db, ud_id)
        # si cote avec expansion
        elif expansion and ud['_id'].startswith(cote) :
            convert_ud(ud, db, ud_id)
        # Sinon, on saute l'ud
        else:
            continue

def resultats(cote=None):
    print(str(compteur.ud_modif) + " uds modifiées")
    print(str(compteur.ud_created) + " uds créées")
    exec_time = datetime.datetime.now() - compteur.begin_time
    print("temps d'exécution : " + str(exec_time))
    if cote and cote != "fiche":
        print("ATTENTION ! si l'UD est nouvelle, il faut mettre à jours son parent pour qu'elle apparaisse dans sa liste d'enfants")
        print("Et éventuellement les autres unités supérieures si les dates ou l'imp. mat. change")

if __name__ == "__main__":
    # Arguments de la commande
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', help="URL de l'inventaire EAD", required=True)
    parser.add_argument('-c', help="Cote à traiter. mentionner 'fiche' son on veut modifier la notice de fonds", required=False)
    parser.add_argument('-e', required=False, action="store_true", help="Etend le traitement aux sous-unités de la cote spécifiée avec -c")
    parser.add_argument('--test',required=False, action="store_true", help="pour le débogage")
    args = parser.parse_args()

    main(args)
