<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:x="http://ead3.archivists.org/schema/undeprecated/" exclude-result-prefixes="x">
  <xsl:output indent="no" method="xml" />
   <!-- <xsl:strip-space elements="*" /> -->
  <xsl:variable name="code_isil" select="x:ead/x:control/x:maintenanceagency/x:agencycode" />
  <xsl:variable name="fonds" select="x:ead/x:control/x:recordid" />
  <xsl:variable name="vedette" select="x:ead//x:control/x:localcontrol[@localtype='vedette']/x:term" />

  <xsl:template match="/">
    <root>
      <xsl:apply-templates select="//x:archdesc" mode="ud" />
      <xsl:apply-templates select="//x:c" mode="ud" />
    </root>
  </xsl:template>

  <xsl:template match="x:archdesc" mode="ud">
    <c>
      <code_isil>
        <xsl:value-of select="$code_isil" />
      </code_isil>
      <fonds>
        <xsl:value-of select="$fonds" />
      </fonds>
      <_id>
        <xsl:value-of select="$fonds" />
      </_id>
      <niveau>
        <xsl:choose>
          <xsl:when test="@level = 'otherlevel'">
            <xsl:value-of select="@otherlevel" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@level" />
          </xsl:otherwise>
        </xsl:choose>
      </niveau>
      <titre>
        <xsl:value-of select="x:did/x:unittitle" />
      </titre>
      <inventaire_pdf><xsl:value-of select="/x:ead/x:control/x:representation/@href"/></inventaire_pdf>
      <inventaire_xml><xsl:value-of select="/x:ead/x:control/x:recordid/@instanceurl"/></inventaire_xml>
      <xsl:apply-templates select="x:did/x:unitid" />
      <xsl:apply-templates select="x:did/x:unitdatestructured/x:datesingle" />
      <xsl:apply-templates select="x:did/x:unitdatestructured/x:daterange" />
      <xsl:apply-templates select="x:did/x:unitdatestructured/x:dateset" />
      <xsl:apply-templates select="x:did/x:physloc" />
      <xsl:apply-templates select="x:did//x:physdescstructured" />
      <xsl:apply-templates select="x:did//x:language"/>
      <xsl:apply-templates select="x:accessrestrict" />
      <xsl:apply-templates select="x:userestrict" />
      <xsl:apply-templates select="x:scopecontent" />
      <xsl:apply-templates select="x:processinfo" />
      <xsl:apply-templates select="x:bioghist" />
      <xsl:apply-templates select="x:custodhist" />
      <xsl:apply-templates select="x:relatedmaterial" />
      <xsl:apply-templates select="x:separatedmaterial" />
      <xsl:apply-templates select="x:otherfindaid" />
      <xsl:apply-templates select="x:acqinfo" />
      <xsl:apply-templates select="x:did/x:materialspec" />
      <xsl:for-each select="x:dsc/x:c">
        <enfants>
          <xsl:value-of select="@id" />
        </enfants>
      </xsl:for-each>
      <vedette><xsl:value-of select="/x:ead/x:control/x:localcontrol[@localtype='vedette']"/></vedette>

    </c>
    <!-- S'il n'y pas d'UD (seulement une fiche fonds), ça fait planter le script de conversion car il n'y a pas plusieurs 'c'-->
    <!-- On rajoute donc une UD vide -->
    <xsl:if test="not(x:dsc/x:c)">
        <c>
            <intitule>Dummy UD</intitule>
        </c>
    </xsl:if>

</xsl:template>

<xsl:template match="x:language">
    <langues><xsl:value-of select="@langcode"/></langues>
</xsl:template>

  <xsl:template match="x:c" mode="ud">
    <c>
      <code_isil>
        <xsl:value-of select="$code_isil" />
      </code_isil>
      <fonds>
        <xsl:value-of select="$fonds" />
      </fonds>
      <_id>
        <xsl:value-of select="@id" />
      </_id>
      <niveau>
        <xsl:choose>
          <xsl:when test="@level = 'otherlevel'">
            <xsl:value-of select="@otherlevel" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@level" />
          </xsl:otherwise>
        </xsl:choose>
      </niveau>
      <titre>
        <xsl:value-of select="x:did/x:unittitle" />
      </titre>
      <xsl:apply-templates select="x:did/x:unitid" />
      <xsl:apply-templates select="x:did/x:unitdatestructured/x:datesingle" />
      <xsl:apply-templates select="x:did/x:unitdatestructured/x:daterange" />
      <xsl:apply-templates select="x:did/x:unitdatestructured/x:dateset" />
      <xsl:apply-templates select="x:did/x:physloc" />
      <xsl:apply-templates select="x:did//x:physdescstructured" />
      <xsl:apply-templates select="x:accessrestrict" />
      <xsl:apply-templates select="x:userestrict" />
      <xsl:apply-templates select="x:scopecontent" />
      <xsl:apply-templates select="x:processinfo" />
      <xsl:apply-templates select="x:bioghist" />
      <xsl:apply-templates select="x:custodhist" />
      <xsl:apply-templates select="x:relatedmaterial" />
      <xsl:apply-templates select="x:separatedmaterial" />
      <xsl:apply-templates select="x:otherfindaid" />
      <xsl:apply-templates select="x:acqinfo" />
      <xsl:apply-templates select="x:did/x:materialspec" />
      <xsl:for-each select="x:c">
        <enfants>
          <xsl:value-of select="@id" />
        </enfants>
      </xsl:for-each>
      <parents>
        <xsl:value-of select="$fonds" />
      </parents>
      <xsl:for-each select="ancestor::x:c">
        <parents>
          <xsl:value-of select="@id" />
        </parents>
      </xsl:for-each>
      <xsl:if test="following-sibling::x:c/@id">
        <ud_suiv>
          <xsl:value-of select="following-sibling::x:c/@id" />
        </ud_suiv>
      </xsl:if>
      <xsl:if test="preceding-sibling::x:c/@id">
        <ud_prec>
          <xsl:value-of select="preceding-sibling::x:c[1]/@id" />
        </ud_prec>
      </xsl:if>
      <sororite_nb>
        <xsl:value-of select="count(../x:c)" />
      </sororite_nb>
      <sororite_rang>
        <xsl:value-of select="count(preceding-sibling::x:c) + 1" />
      </sororite_rang>
      <xsl:if test="not(x:c)">
        <docs_num_acces>
          <xsl:value-of select="ancestor-or-self::x:*[x:accessrestrict]/x:accessrestrict/@localtype"/>
        </docs_num_acces>
      </xsl:if>



    </c>
  </xsl:template>

<xsl:template name="acces-doc-num">
  <docs_num_acces>
    <xsl:value-of select="ancestor-or-self::x:c[x:accessrestrict]/@localtype"/>
  </docs_num_acces>

</xsl:template>

  <xsl:template match="x:emph[@render='italic']">
    <xsl:text> *</xsl:text>
    <xsl:apply-templates />
    <xsl:text>* </xsl:text>
  </xsl:template>

  <xsl:template match="x:emph[@render='bold']">
    <xsl:text> **</xsl:text>
    <xsl:apply-templates />
    <xsl:text>** </xsl:text>
  </xsl:template>

  <xsl:template match="x:ref[@linkrole='refFonds']">
    <xsl:text> [</xsl:text>
    <xsl:value-of select="." />
    <xsl:text>](</xsl:text>
    <xsl:choose>
      <xsl:when test="contains(@href,'/')">
        <xsl:value-of select="substring-after(@href,'/')" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="@href" />
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>) </xsl:text>
  </xsl:template>

  <xsl:template match="x:ref[@linkrole='refBiblio']">
    <xsl:text> [</xsl:text>
    <xsl:value-of select="." />
    <xsl:text>](../ouvrage/</xsl:text>
    <xsl:value-of select="@href" />
    <xsl:text>) </xsl:text>
  </xsl:template>

  <xsl:template match="x:ref[@linkrole='refTranscription']">
    <xsl:text> [</xsl:text>
    <xsl:value-of select="." />
    <xsl:text>](../doc/</xsl:text>
    <xsl:value-of select="@href" />
    <xsl:text>) </xsl:text>
  </xsl:template>

  <xsl:template match="x:ref[@linkrole='refDossier']">
    <xsl:text> [</xsl:text>
    <xsl:value-of select="." />
    <xsl:text>](/page/</xsl:text>
    <xsl:value-of select="@href" />
    <xsl:text>) </xsl:text>
  </xsl:template>

  <xsl:template match="x:ref[@linkrole='query']">
    <xsl:text> [</xsl:text>
    <xsl:value-of select="." />
    <xsl:text>](../search?</xsl:text>
    <xsl:value-of select="@href" />
    <xsl:text>) </xsl:text>
  </xsl:template>

  <xsl:template match="x:ref[@target]">
    <xsl:text> [</xsl:text>
    <xsl:value-of select="." />
    <xsl:text>](</xsl:text>
    <xsl:value-of select="@target" />
    <xsl:text>) </xsl:text>
  </xsl:template>

  <xsl:template match="x:ref[@linkrole='externe']">
    <xsl:text> [</xsl:text>
    <xsl:value-of select="." />
    <xsl:text>](</xsl:text>
    <xsl:value-of select="@href" />
    <xsl:text>) </xsl:text>
  </xsl:template>

  <xsl:template match="x:p">
      <xsl:apply-templates />
      <xsl:text>&#xa;&#xa;</xsl:text>
  </xsl:template>

  <xsl:template match="x:list">
    <xsl:for-each select="x:item">
      <xsl:text>- </xsl:text>
      <xsl:apply-templates />
      <xsl:text>&#xa;</xsl:text>
    </xsl:for-each>
      <xsl:text>&#xa;&#xa;</xsl:text>
  </xsl:template>

  <xsl:template match="x:materialspec[@label = 'Type de document']">
    <doctype>
      <xsl:value-of select="." />
    </doctype>
  </xsl:template>

  <xsl:template match="x:materialspec[@label = 'Durée [min]']">
    <duree_min>
      <xsl:value-of select="." />
    </duree_min>
  </xsl:template>

  <xsl:template match="x:materialspec[@label = 'Couleur']">
    <couleur>
      <xsl:value-of select="." />
    </couleur>
  </xsl:template>

  <xsl:template match="x:materialspec[@label = 'Taille [mm]']">
    <dimension_mm>
      <xsl:value-of select="." />
    </dimension_mm>
  </xsl:template>

  <xsl:template match="x:accessrestrict">
    <acces>
      <xsl:apply-templates />
    </acces>
  </xsl:template>

  <xsl:template match="x:userestrict[@localtype='licence']">
    <licence>
      <xsl:apply-templates />
    </licence>
  </xsl:template>
  <xsl:template match="x:userestrict[@localtype='auteur']">
    <auteur>
      <xsl:apply-templates />
    </auteur>
  </xsl:template>

  <!-- For legacy inventories START -->
  <xsl:template match="x:userestrict[@localtype='Copyright']">
      <copyright><xsl:apply-templates/></copyright>
  </xsl:template>
  <!-- For legacy inventories END -->

  <xsl:template match="x:scopecontent">
    <scopecontent>
      <xsl:apply-templates />
    </scopecontent>
  </xsl:template>

  <xsl:template match="x:processinfo">
    <processinfo>
      <xsl:apply-templates />
    </processinfo>
  </xsl:template>

  <xsl:template match="x:bioghist">
    <bioghist>
      <xsl:apply-templates />
    </bioghist>
  </xsl:template>

  <xsl:template match="x:custodhist">
    <custodhist>
      <xsl:apply-templates />
    </custodhist>
  </xsl:template>

  <xsl:template match="x:otherfindaid">
    <otherfindaid>
      <xsl:apply-templates />
    </otherfindaid>
  </xsl:template>

  <xsl:template match="x:relatedmaterial">
    <relatedmaterial>
      <xsl:apply-templates />
    </relatedmaterial>
  </xsl:template>

  <xsl:template match="x:separatedmaterial">
    <separatedmaterial>
      <xsl:apply-templates />
    </separatedmaterial>
  </xsl:template>

  <xsl:template match="x:acqinfo">
    <acqinfo>
      <xsl:apply-templates />
    </acqinfo>
  </xsl:template>

  <xsl:template match="x:userestrict">
    <copyright>
      <xsl:apply-templates />
    </copyright>
  </xsl:template>

  <xsl:template match="x:physdescstructured">
    <impmat>
      <xsl:value-of select="x:quantity" />
      <xsl:text> </xsl:text>
      <xsl:value-of select="x:unittype" />
    </impmat>
  </xsl:template>

  <xsl:template match="x:physloc">
    <loc>
      <xsl:value-of select="." />
    </loc>
  </xsl:template>

  <xsl:template match="x:unitid">
    <cotes>
      <type>
        <xsl:value-of select="@label" />
      </type>
      <val>
        <xsl:value-of select="." />
      </val>
    </cotes>
  </xsl:template>

  <xsl:template match="x:datesingle">
    <dates>
      <xsl:value-of select="@standarddate" />
      <xsl:if test="not(@standarddate)">
        <xsl:text>[s.d.]</xsl:text>
      </xsl:if>
    </dates>
  </xsl:template>

  <xsl:template match="x:daterange">
    <xsl:if test="x:fromdate">
      <dates>
        <xsl:value-of select="x:fromdate/@standarddate" />
        <xsl:text>/</xsl:text>
        <xsl:value-of select="x:todate/@standarddate" />
      </dates>
    </xsl:if>

  </xsl:template>

  <xsl:template match="x:dateset">
    <xsl:apply-templates />
</xsl:template>

    <!-- Pour éviter que des tabul ou des retour à ligne à l'intérieur des <p> ne soient recrées -->
    <xsl:template match="text()">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>

    <xsl:template match="x:chronlist">
        <xsl:text>| | |&#xa;|-|-|&#xa;</xsl:text>
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="x:chronitem">
        <xsl:text>| </xsl:text><xsl:value-of select="x:datesingle"/><xsl:text> | </xsl:text><xsl:value-of select="x:event"/><xsl:text> |&#xa;</xsl:text>
    </xsl:template>

</xsl:stylesheet>
