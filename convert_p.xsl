
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ns0="http://ead3.archivists.org/schema/">
<xsl:output method="html"/>



<xsl:template match="/">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ns0:emph[@render='italic']">
  <xsl:text>*</xsl:text>
  <xsl:apply-templates />
  <xsl:text>*</xsl:text>
</xsl:template>

<xsl:template match="ns0:emph[@render='bold']">
  <xsl:text>**</xsl:text>
  <xsl:apply-templates />
  <xsl:text>**</xsl:text>
</xsl:template>

<xsl:template match="ns0:ref[@linkrole='refFonds']">
  <xsl:text>[</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text>](</xsl:text>
  <xsl:choose>
      <xsl:when test="contains(@href,'/')">
        <xsl:value-of select="substring-after(@href,'/')"/>
    </xsl:when>
    <xsl:otherwise>
        <xsl:value-of select="@href"/>
    </xsl:otherwise>
</xsl:choose>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ns0:ref[@linkrole='refBiblio']">
  <xsl:text>[</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text>](../ouvrage/</xsl:text>
  <xsl:value-of select="@href"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ns0:ref[@linkrole='refTranscription']">
  <xsl:text>[</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text>](../doc/</xsl:text>
  <xsl:value-of select="@href"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ns0:ref[@linkrole='query']">
  <xsl:text>[</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text>](../search?</xsl:text>
  <xsl:value-of select="@href"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ns0:ref[@target]">
  <xsl:text>[</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text>](</xsl:text>
  <xsl:value-of select="@target"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ns0:ref[@linkrole='externe']">
  <xsl:text>[</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text>](</xsl:text>
  <xsl:value-of select="@href"/>
  <xsl:text>)</xsl:text>
</xsl:template>

</xsl:stylesheet>
